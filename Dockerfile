FROM php:5.6-apache 

RUN docker-php-ext-install mysqli

COPY ci/bonfire.conf /etc/apache2/sites-available/bonfire.conf


COPY www/ /var/www/html
COPY ci/permissions.sh /usr/bin/permissions.sh


RUN apt-get update && apt-get install nano

RUN a2dissite 000-default.conf
RUN a2ensite bonfire.conf
RUN a2enmod rewrite
