<?php

Assets::add_js(array('bootstrap.min.js', 'jwerty.js', "adminlte.min.js", "jquery.overlayScrollbars.min.js"), 'external', true);

echo theme_view('header');

?>
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <?php if (isset($toolbar_title)) : ?>
                        <h4 class="m-0 text-dark"><?php echo $toolbar_title; ?></h4>
                    <?php endif; ?>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <?php Template::block('sub_nav', ''); ?>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="content">
        <div class="container-fluid">
            <?php
                echo Template::message();
                echo isset($content) ? $content : Template::content();
            ?>
        </div>
    </div>
</div>
<?php echo theme_view('footer'); ?>