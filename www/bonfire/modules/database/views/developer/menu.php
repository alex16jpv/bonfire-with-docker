<?php $backups = $this->uri->segment(3) == "database" && $this->uri->segment(4) == 'backups' ? "active": ""; ?>
<?php $home = $this->uri->segment(3) == "database" && $this->uri->segment(4) == '' ? "active": ""; ?>
<li class="nav-item"><a class="nav-link <?php echo $home ?>" href="<?php echo site_url(SITE_AREA . '/developer/database'); ?>"><?php echo lang('bf_menu_db_maintenance'); ?></a></li>
<li class="nav-item"><a class="nav-link <?php echo $backups ?>" href="<?php echo site_url(SITE_AREA . '/developer/database/backups'); ?>"><?php echo lang('bf_menu_db_backup'); ?></a></li>