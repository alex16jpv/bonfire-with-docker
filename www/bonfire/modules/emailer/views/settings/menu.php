<?php $template = $this->uri->segment(3) == "emailer" && $this->uri->segment(4) == 'template' ? "active": ""; ?>
<?php $queue = $this->uri->segment(3) == "emailer" && $this->uri->segment(4) == 'queue' ? "active": ""; ?>
<?php $home = $this->uri->segment(3) == "emailer" && $this->uri->segment(4) == '' ? "active": ""; ?>

<li class="nav-item"><a class="nav-link <?php echo $home ?>" href="<?php echo site_url(SITE_AREA . '/settings/emailer'); ?>"><?php echo lang('bf_menu_email_settings'); ?></a></li>
<li class="nav-item"><a class="nav-link <?php echo $template ?>" href="<?php echo site_url(SITE_AREA . '/settings/emailer/template'); ?>"><?php echo lang('bf_menu_email_template'); ?></a></li>
<li class="nav-item"><a class="nav-link <?php echo $queue ?>" href="<?php echo site_url(SITE_AREA . '/settings/emailer/queue'); ?>"><?php echo lang('bf_menu_email_queue'); ?></a></li>