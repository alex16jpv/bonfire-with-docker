## Remove index.php

You need to modify application/config/config.php

```php
// Change

$config['index_page'] = "index.php";
$config['uri_protocol'] = "AUTO";

// To

$config['index_page'] = '';
$config['uri_protocol'] = "REQUEST_URI";
```
